from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Receipt, ExpenseCategory, Account
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.
# Get all of the instances of the Receipt model
# and put them in the context for the template.
# This list view is only accessible if the user is logged in.
@login_required
def receipt_list(request):
    list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receiptlist_contextkey": list,
    }
    return render(request, "receipts/receiptlist.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            newreceipt = form.save(False)
            newreceipt.purchaser = request.user
            newreceipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/newreceipt.html", context)


# Display a list of expense categories for the logged in user
@login_required
def expenses_list(request):
    list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categorylist_contextkey": list,
    }
    return render(request, "receipts/categorylist.html", context)


# Display a list of accounts for the logged in user
@login_required
def accounts_list(request):
    list = Account.objects.filter(owner=request.user)
    context = {
        "accountlist_contextkey": list,
    }
    return render(request, "receipts/accountlist.html", context)


# Allow the logged in user to create a new expense category.
@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            newcategory = form.save(False)
            newcategory.owner = request.user
            newcategory.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/createcategory.html", context)


# Allow the logged in user to create a new account for the account view
@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            newaccount = form.save(False)
            newaccount.owner = request.user
            newaccount.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/createaccount.html", context)
